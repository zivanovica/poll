<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPollAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users_poll_answers', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id');

            $table->integer('poll_id');

            $table->integer('poll_answer_id');

            $table->timestamp('created_at')->nullable();

            $table->timestamp('updated_at')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('users_poll_answers');

    }
}
