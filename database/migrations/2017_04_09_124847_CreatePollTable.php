<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('creator_id');

            $table->boolean('is_active')->default(false);

            $table->boolean('is_public')->default(false);

            $table->string('title', 256);

            $table->text('question');

            $table->timestamp('created_at')->nullable();

            $table->timestamp('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('polls');

    }
}
