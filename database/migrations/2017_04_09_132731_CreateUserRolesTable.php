<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('user_roles', function (Blueprint $table) {

            $table->increments('id');

            $table
                ->enum('role_type', \App\Models\UserRole::getAvailableRoles())
                ->default(\App\Models\UserRole::getDefaultRole())
            ;

            $table->integer('user_id')->unique();

            $table->timestamp('created_at')->nullable();

            $table->timestamp('updated_at')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExsits('user_roles');

    }
}
