@extends('layouts.app')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        @if (false === empty($errorMessage))
            <div class="alert alert-danger">
                {{ $errorMessage }}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (false === empty($message))
            <div class="alert alert-info">
                {{ $message }}
            </div>
        @endif
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr
                        id="user-data-{{ $user->id }}"
                        data-href="{{ route('admin.user.save', ['user' => $user ]) }}"
                        data-csrf="{{ csrf_token() }}"
                    >
                        <td>
                            {{ $user->id }}
                        </td>
                        <td>
                            <input
                                type="text"
                                name="email"
                                class="email"
                                value="{{ $user->email }}"
                            />
                        </td>
                        <td>
                            <select
                                class="role"
                                name="role"
                            >
                                @foreach(\App\Models\UserRole::getAvailableRoles() as $role)
                                    <option
                                        value="{{ $role }}"
                                        {{ $user->role->role_type === $role ? 'selected' : ''}}
                                    >
                                        {{ $role }}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select
                                class="status"
                                name="status"
                            >
                                @foreach(\App\User::getStatuses() as $status)
                                    <option
                                        value="{{ $status }}"
                                        {{ $status === $user->status ? 'selected' : ''}}
                                    >
                                        {{ $status }}
                                    </option>

                                @endforeach
                            </select>
                        </td>
                        <td>
                            <button class="btn btn-primary save" value="{{ $user->id }}">Save</button>
                            <a
                                href="{{ route('admin.user.delete', ['user' => $user]) }}"
                                class="btn btn-danger remove"
                            >
                                Remove
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('javascript.after')
    <script type="text/javascript">

        window.onload = function () {

            var buttons = document.querySelectorAll('button.save');

            if (typeof buttons.forEach !== 'function') {

                return;

            }

            buttons.forEach(function (button) {

                button.addEventListener('click', function (event) {

                    event.preventDefault();

                    event.stopImmediatePropagation();

                    var user = document.querySelector('#user-data-' + button.value);

                    user.style.display = 'none';

                    var form = document.createElement('form');

                    form.appendChild(user.querySelector('input.email'));

                    form.appendChild(user.querySelector('select.role'));

                    form.appendChild(user.querySelector('select.status'));

                    user.appendChild(form);

                    form.method = 'post';

                    form.action = user.dataset.href + '?_token=' + user.dataset.csrf;

                    form.submit();

                });

            });

        };

    </script>
@endsection