@extends('layouts.app')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="{{ route('admin.poll.update', ['poll' => $poll])  }}" class="form">
            <div class=" panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        Edit Poll: {{ $poll->title }}
                    </h2>
                </div>
                <div class="panel-body">
                    @if (false === empty($errorMessage))
                        <div class="alert alert-danger">
                            {{ $errorMessage  }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input
                            type="text"
                            name="title"
                            value="{{ $poll->title }}"
                            class="form-control"
                            placeholder="Title"
                            required
                    /><br />
                    <textarea
                            name="question"
                            class="form-control"
                            placeholder="Question?"
                            required
                    >{{ $poll->question }}</textarea><br />
                    <br />
                    <div class="answers-holder">
                        <ul class="list-group" id="answers">
                            @foreach($poll->answers as $answer)
                                <li class="list-group-item" id="answer-holder-{{ $answer->id }}">
                                    <div class="input-group">
                                        <input
                                            type="text"
                                            name="answer[{{ $answer->id }}]"
                                            class="form-control"
                                            value="{{ $answer->answer  }}"
                                            {{ false === $edit_answers ? 'disabled' : '' }}
                                        />
                                        <span class="input-group-btn">
                                            <button
                                                class="remove btn btn-default"
                                                value="{{ $answer->id }}"
                                                {{ $edit_answers ? '' : 'disabled' }}
                                            >
                                                Remove
                                            </button>
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    {{ csrf_field() }}
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-default">Save Poll</button>
                    <button
                            type="button"
                            class="btn btn-primary new-answer"
                            {{ $edit_answers ? '' : 'disabled' }}
                    >Add Answer</button>
                    <div class="pull-right">
                        <label for="is_public">
                            Public Results
                        </label>
                        <input
                            type="checkbox"
                            name="is_public"
                            id="is_public"
                            {{ $poll->is_public ? 'checked' : '' }}
                        />
                        <label for="is_active">
                            Poll is active
                        </label>
                        <input
                            type="checkbox"
                            name="is_active"
                            id="is_active"
                            {{ $poll->is_active ? 'checked' : '' }}
                        />
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript.after')

    <script type="text/javascript">

        (function () {

            var createAnswerElement = function () {

                var li = document.createElement('li');

                li.classList.add('list-group-item');

                var input = document.createElement('input');

                input.classList.add('form-control');

                input.placeholder = 'Answer...';

                input.setAttribute('name', 'answer[new][]');

                li.appendChild(input);

                return li;

            };

            var removeAnswer = function (id) {

                if (false === confirm('Are you sure?')) {

                    return;

                }

                var input = document.querySelector('#answer-holder-' + id);

                if (typeof input.remove !== 'function') {

                    return;

                }

                input.remove();

            };

            var bindEvents = function () {

                var answersHolderElement = document.querySelector('#answers');

                var addAnswerButton = document.querySelector('button.new-answer');

                addAnswerButton.addEventListener('click', function (event) {

                    event.preventDefault();

                    event.stopImmediatePropagation();

                    answersHolderElement.appendChild(createAnswerElement());

                });

                var removeAnswerButtons = document.querySelectorAll('button.remove');

                if (typeof removeAnswerButtons.length === 'undefined' || 0 === removeAnswerButtons.length)
                {

                    return;

                }

                removeAnswerButtons.forEach(function (button) {

                    button.addEventListener('click', function (event) {

                        event.preventDefault();

                        event.stopImmediatePropagation();

                        removeAnswer(Number.parseInt(button.value));

                    });

                });

            };

            window.onload = bindEvents;

        })();

    </script>

@endsection