@extends('layouts.app')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="{{ route('admin.poll.insert')  }}" class="form">
            <div class=" panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        Create New Poll:
                    </h2>
                </div>
                <div class="panel-body">
                    @if (false === empty($errorMessage))
                        <div class="alert alert-danger">
                            {{ $errorMessage  }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                        <input
                                type="text"
                                name="title"
                                value="{{ empty($title) ? '' : $title }}"
                                class="form-control"
                                placeholder="Title"
                                required
                        /><br />
                        <textarea
                                name="question"
                                class="form-control"
                                placeholder="Question?"
                                required
                        >{{ empty($question) ? '' : $question }}</textarea><br />
                        <br />
                        <div class="answers-holder">
                            <ul class="list-group" id="answers">
                                <li class="list-group-item">
                                    <input
                                            type="text"
                                            name="answer[]"
                                            class="form-control"
                                            placeholder="#1 Answer"
                                            value="{{ empty($answers[0]) ? '' : $answers[0]  }}"
                                    />
                                </li>
                                <li class="list-group-item">
                                    <input
                                            type="text"
                                            name="answer[]"
                                            class="form-control"
                                            placeholder="#2 Answer"
                                            value="{{ empty($answers[0]) ? '' : $answers[0]  }}"
                                    />
                                </li>
                            </ul>
                        </div>

                        {{ csrf_field() }}
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-default">Create Poll</button>
                    <button
                            type="button"
                            class="btn btn-primary new-answer"
                    >Add Answer</button>
                    <div class="pull-right">
                        <label for="is_public">
                            Public Results
                        </label>
                        <input
                                type="checkbox"
                                name="is_public"
                                id="is_public"
                                {{ (empty($is_public) || false === $is_public) ? '' : 'checked' }}
                        />
                        <label for="is_active">
                            Poll is active
                        </label>
                        <input
                                type="checkbox"
                                name="is_active"
                                id="is_active"
                                {{ (empty($is_active) || false === $is_active) ? '' : 'checked' }}
                        />
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript.after')

    <script type="text/javascript">

        (function () {

            var createAnswerElement = function () {

                var li = document.createElement('li');

                li.classList.add('list-group-item');

                var input = document.createElement('input');

                input.classList.add('form-control');

                input.placeholder = '#' + (document.querySelectorAll('[name="answer[]"]').length + 1) + ' Answer';

                input.setAttribute('name', 'answer[]');

                li.appendChild(input);

                return li;

            };

            var removeAnswer = function (id) {

                if (false === confirm('Are you sure?')) {

                    return;

                }

                var input = document.querySelector('#answer-holder-' + id);

                if (typeof input.remove !== 'function') {

                    return;

                }

                input.remove();

            };

            var bindEvents = function () {

                var answersHolderElement = document.querySelector('#answers');

                var addAnswerButton = document.querySelector('button.new-answer');

                addAnswerButton.addEventListener('click', function (event) {

                    event.preventDefault();

                    event.stopImmediatePropagation();

                    answersHolderElement.appendChild(createAnswerElement());

                });

            };

            window.onload = bindEvents;

        })();

    </script>

@endsection