@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <ul class="list-group polls">
            @foreach($polls as $poll)
                <li class="list-group-item">
                    <a href="{{ route('poll', ['poll' => $poll]) }}">
                        {{ $poll->title  }}
                    </a>

                    <div class="pull-right">
                        <a
                                href="{{ route('poll.results', ['poll' => $poll]) }}"
                                class="btn btn-success btn-sm"
                        >
                            Results
                        </a>
                        <a
                                href="{{ route('admin.poll.edit', ['poll' => $poll]) }}"
                                class="btn btn-info btn-sm"
                        >Edit</a>
                        <a
                            href="{{ route('admin.poll.delete', ['poll' => $poll]) }}"
                            class="btn btn-danger btn-sm"
                        >
                            Remove
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection