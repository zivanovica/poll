<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Polls</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/js/jquery-3.2.0.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.js"></script>

    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        @yield('javascript.after')
    </body>
</html>
