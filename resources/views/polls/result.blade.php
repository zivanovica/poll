@extends('layouts.app')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ $poll->title }}
                </h3>
            </div>
            <div class="panel-body">
                {{ $poll->question }}
                <ul class="list-group">
                    @foreach($poll->answers as $answer)
                        <li class="list-group-item">
                            {{ $answer->answer }}
                            <div class="pull-right">
                                {{ $answer->percentage }}%
                                <span class="badge badge-info">{{ $answer->totalVotes }} vote(s)</span>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection