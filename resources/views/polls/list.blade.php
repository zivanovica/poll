@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        @foreach($polls as $poll)
            <form method="post" action="{{ route('poll.vote', ['poll' => $poll]) }}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            {{ $poll->title }}
                        </div>
                    </div>
                    <div class="panel-body">
                        {{ $poll->question }}

                        <ul class="list-group">
                            @foreach($poll->answers as $answer)
                                <li class="list-group-item">
                                    <label for="answer-{{ $answer->id }}">
                                        {{ $answer->answer }}
                                    </label>
                                    <input
                                        type="radio"
                                        name="answer"
                                        value="{{ $answer->id }}"
                                        id="answer-{{ $answer->id }}"
                                        class="pull-right"
                                    />
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <button type="submit">Submit</button>
                    </div>
                </div>
                {{ csrf_field() }}
            </form>
        @endforeach
    </div>
@endsection