<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {

    Route::get('/users', 'AdminController@users')->name('admin.users');

    Route::get('/delete/{user}', 'AdminController@deleteUser')->name('admin.user.delete');

    Route::post('/users/{user}', 'AdminController@editUser')->name('admin.user.save');


    Route::get('/polls', 'AdminController@polls')->name('admin.poll.list');

    Route::get('/poll', 'AdminController@newPoll')->name('admin.poll.new');

    Route::get('/poll/{poll}', 'AdminController@editPoll')->name('admin.poll.edit');

    Route::get('/poll/{poll}/delete', 'AdminController@delete')->name('admin.poll.delete');

    Route::post('/poll/{poll}', 'AdminController@updatePoll')->name('admin.poll.update');

    Route::post('/poll', 'AdminController@insertPoll')->name('admin.poll.insert');

});

Route::group(['middleware' => 'auth', 'prefix' => 'polls'], function () {

    Route::get('/', 'PollsController@polls')->name('poll.list');

    Route::get('/results', 'PollsController@userResults')
        ->name('poll.results.user')
    ;

    Route::get('/{poll}', 'PollsController@poll')
        ->middleware(\App\Http\Middleware\PollVoteAvailable::class)
        ->name('poll')
    ;

    Route::get('/{poll}/results', 'PollsController@pollResults')
        ->middleware(\App\Http\Middleware\PollPublic::class)
        ->name('poll.results')
    ;

    Route::post('/poll/{poll}', 'PollsController@vote')
        ->middleware(\App\Http\Middleware\PollVoteAvailable::class)
        ->name('poll.vote')
    ;

});

Auth::routes();

Route::get('/activation/{activation_code}', 'Auth\RegisterController@activate')->name('user.activate');

Route::get('/home', 'HomeController@index');
