<?php

namespace App\Providers;

use App\Models\Poll;
use App\Models\UserRole;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('poll-can-vote', function (User $user, Poll $poll) {

            return
                $user->isAdmin() ||
                ($poll instanceof Poll && $poll->usersAnswers->where('user_id', $user->id)->isEmpty() && $poll->is_active)
            ;

        });
    }
}
