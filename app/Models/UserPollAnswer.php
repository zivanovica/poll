<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPollAnswer extends Model
{

    protected $table = 'users_poll_answers';

    protected $fillable = [

        'user_id',

        'poll_id',

        'poll_answer_id'

    ];

    public function poll()
    {

        return $this->belongsTo(Poll::class, 'id', 'poll_id');

    }

    public function user()
    {

        return $this->belongsTo(User::class, 'id', 'user_id');

    }

}
