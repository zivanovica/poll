<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    const ROLE_USER = 'user';

    const ROLE_ADMIN = 'admin';

    protected $table = 'user_roles';

    protected $fillable = [

        'role_type',

        'user_id'

    ];

    public static function getAvailableRoles()
    {

        return [

            UserRole::ROLE_USER,

            UserRole::ROLE_ADMIN

        ];

    }

    public static function getDefaultRole()
    {

        return UserRole::ROLE_USER;

    }

    public function user()
    {

        return $this->belongsTo(User::class, 'user_id', 'id');

    }


}
