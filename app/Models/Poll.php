<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Poll extends Model
{

    const ERROR_NONE = 0;

    const ERROR_POLL_CREATING = 1;

    const ERROR_ANSWERS_CREATING = 2;

    const ERROR_POLL_NOT_FOUND = 3;

    const INPUT_TITLE = 'title';

    const INPUT_QUESTION = 'question';

    const INPUT_ACTIVE = 'is_active';

    const INPUT_PUBLIC = 'is_public';

    const INPUT_CREATOR = 'creator_id';

    protected $table = 'polls';

    protected $fillable = [

        'creator_id',

        'is_active',

        'is_public',

        'title',

        'question'

    ];

    public static function getUserPollResults(User $user)
    {
        // Retrieve polls that are public and user voted on
        return Poll::where('is_public', true)->whereIn('id', $user->answers->pluck('poll_id')->toArray())->get();
    }

    public static function getUserVotablePolls(User $user)
    {
        // Retrieve polls that are active and user hasn't answered yet, if user is administrator empty array is returned
        return
            $user->isAdmin() ?
                [] :
                Poll::where('is_active', true)->whereNotIn('id', $user->answers->pluck('poll_id')->toArray())->get();
    }

    public function createUserVote(User $user, PollAnswer $answer)
    {

        $userAnswer = new UserPollAnswer([

            'user_id' => $user->id,

            'poll_id' => $this->id,

            'poll_answer_id' => $answer->id

        ]);

        return $userAnswer;

    }

    public function answers()
    {

        return $this->hasMany(PollAnswer::class, 'poll_id', 'id');

    }

    public function usersAnswers()
    {

        return $this->hasMany(UserPollAnswer::class, 'poll_id', 'id');

    }

    /**
     * @param array $pollData
     * @param array $answers
     * @return Poll|int In case that return value is integer, refer to "\App\Models\Poll" constants
     */
    public static function createPollWithAnswers(array $pollData, array $answers)
    {

        $poll = new Poll([

            'title' => isset($pollData[Poll::INPUT_TITLE]) ? $pollData[Poll::INPUT_TITLE] : null,

            'question' => isset($pollData[Poll::INPUT_QUESTION]) ? $pollData[Poll::INPUT_QUESTION] : null,

            'is_active' => isset($pollData[Poll::INPUT_ACTIVE]) ? $pollData[Poll::INPUT_ACTIVE] : null,

            'is_public' => isset($pollData[Poll::INPUT_PUBLIC]) ? $pollData[Poll::INPUT_PUBLIC] : null,

            'creator_id' => isset($pollData[Poll::INPUT_CREATOR]) ? $pollData[Poll::INPUT_CREATOR] : null,

        ]);

        if (false === $poll->save()) {

            return Poll::ERROR_POLL_CREATING;

        }

        $answersStatus = $poll->createPollAnswers($answers);

        if (Poll::ERROR_NONE !== $answersStatus) {

            return $answersStatus;

        }

        return $poll;
    }

    /**
     * @param array $answers
     * @return int
     */
    public function createPollAnswers(array $answers)
    {

        if (false === $this->exists) {

            return Poll::ERROR_POLL_NOT_FOUND;

        }

        $createdAnswers = [];

        // TODO: Investigate bulk save to avoid multiple insert calls
        foreach ($answers as $answer) {

            $pollAnswer = new PollAnswer([

                'poll_id' => $this->id,

                'answer' => $answer
            ]);

            if ($pollAnswer->save()) {

                $createdAnswers[] = $pollAnswer;

            } else {

                $this->clearCreatedAnswers($createdAnswers);

                return Poll::ERROR_ANSWERS_CREATING;

            }
        }

        return Poll::ERROR_NONE;

    }

    /**
     * @param PollAnswer[] $answers
     */
    private function clearCreatedAnswers(array $answers)
    {

        foreach ($answers as $answer) {

            $answer->delete();

        }

    }

    /**
     *
     * Updates existing answers and creates new one
     *
     * @param Request $request
     */
    public function updateAnswers(Request $request)
    {

        $existingAnswers = $request->get('answer', []);

        $newAnswers = empty($existingAnswers['new']) ? [] : $existingAnswers['new'];

        unset($existingAnswers['new']);

        $this->updateExistingAnswers($existingAnswers);

        $this->insertNewAnswers($newAnswers);

    }

    /**
     *
     * Update or remove existing poll answers
     *
     * @param array $inputAnswers
     */
    public function updateExistingAnswers(array $inputAnswers)
    {

        /** @var PollAnswer $answer */
        foreach ($this->answers as $answer) {

            $inputAnswer = isset($inputAnswers[$answer->id]) ? $inputAnswers[$answer->id] : null;

            if (null === $inputAnswer) {

                $answer->delete();

                continue;

            }

            if (strcmp($inputAnswer, $answer->answer)) {

                $answer->answer = $inputAnswer;

                $answer->update();

                continue;

            }

        }

    }

    /**
     *
     * Create new poll answers
     *
     * @param array $answers
     */
    public function insertNewAnswers(array $answers)
    {

        foreach ($answers as $answer) {

            if (empty($answer)) {

                continue;

            }

            $pollAnswer = new PollAnswer([

                'poll_id' => $this->id,

                'answer' => $answer

            ]);

            $pollAnswer->save();

        }

    }
}
