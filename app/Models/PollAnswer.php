<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PollAnswer extends Model
{

    protected $table = 'poll_answers';

    public $percentage;

    protected $fillable = [

        'poll_id',

        'answer'

    ];

    /**
     * @return Poll
     */
    public function poll()
    {

        return $this->belongsTo(Poll::class, 'poll_id', 'id');

    }

    /**
     * @return UserPollAnswer[]
     */
    public function answered()
    {

        return $this->hasMany(UserPollAnswer::class, 'poll_answer_id', 'id');

    }

}
