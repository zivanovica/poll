<?php
/**
 * Created by PhpStorm.
 * User: coaps
 * Date: 9.4.2017.
 * Time: 15.03
 */

namespace App\Http\Controllers;

use App\Models\Poll;
use App\Models\PollAnswer;
use App\Models\UserPollAnswer;
use App\Models\UserRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class AdminController extends Controller
{

    public function users()
    {

        $users = User::all();

        return view('admin.users', ['users' => $users]);

    }


    /**
     *
     * Handles POST request on /admin/users route, and saves new user data
     *
     * @param Request $request
     * @param User $editingUser
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function editUser(Request $request, User $editingUser)
    {

        $newEmail = strcasecmp($request->get('email', $editingUser->email), $editingUser->email);

        $rules = [

            'email' => ($newEmail ? 'required|unique:users|' : '' ).'email|max:128',

            'role' => 'required|in:' . implode(',' , UserRole::getAvailableRoles()),

            'status' => 'required|in:' . implode(',', User::getStatuses())

        ];

        $validator = $this->getValidationFactory()->make($request->all(), $rules);

        if ($validator->fails()) {

            return view('admin.users', ['users' => User::all(), 'errors' => $validator->getMessageBag()]);

        }

        if ($newEmail) {

            $editingUser->email = $request->get('email');

        }

        $editingUser->status = $request->get('status', $editingUser->status);

        $userRole = $editingUser->role;

        $userRole->role_type = $request->get('role', $userRole->role_type);

        if ($editingUser->save() && $userRole->save()) {

            return view('admin.users', ['message' => 'User updated.', 'users' => User::all()]);

        }

        return view('admin.users', ['errorMessage' => 'Failed to update user.', 'users' => User::all()]);

    }

    public function deleteUser(Request $request, User $user)
    {

        if ($user->id === $request->user()->id) {

            return view('admin.users', ['errorMessage' => 'Can\'t remove yourself.', 'users' => User::all()]);

        }

        if ($user->isAdmin()) {

            return view('admin.users', ['errorMessage' => 'Can\'t remove admin user.', 'users' => User::all()]);

        }

        if ($user->role->delete() && $user->delete()) {

            return view('admin.users', ['message' => 'User removed.', 'users' => User::all()]);

        }

        return view('admin.users', ['errorMessage' => 'Failed to remove user.', 'users' => User::all()]);

    }

    /**
     *
     * Method that handles GET request to list all existing polls
     *
     * @param Request $request
     * @param Response $response
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function polls(Request $request, Response $response)
    {

        return view('admin.polls', ['polls' => Poll::all()]);

    }

    // =============== CREATE POLL =======================

    /**
     *
     * Handles GET request to show new poll form
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function newPoll()
    {

        return view('admin.create_poll');

    }

    /**
     *
     * Handles POST request and creates new poll with answers
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function insertPoll(Request $request)
    {

        $this->validatePollInput($request);

        list($data, $answers) = $this->getNewPollInput($request);

        $poll = null;

        if ($gotAnswers = (count($answers) >= 2)) {

            $poll = Poll::createPollWithAnswers($data, $answers);

        }

        if (false === $poll instanceof Poll || false === $poll->exists) {

            return view('admin.create_poll', array_merge(
                $data,
                [

                    'errorMessage' => false == $gotAnswers ? 'Minimum 2 answers are required' : null,

                    'answers' => $answers

                ]
            ));

        }

        return redirect()->route('admin.poll.edit', ['poll' => $poll]);
    }

    /**
     *
     * Retrieve input data formatted inserting new poll
     *
     *
     * @param Request $request
     * @return array [0 => poll data, 1 => answers]
     */
    private function getNewPollInput(Request $request)
    {

        $data = [

            'title' => $request->get('title'),

            'question' => $request->get('question'),

            'is_active' => 0 === strcasecmp($request->get('is_active', 'off'), 'on'),

            'is_public' => 0 === strcasecmp($request->get('is_public', 'off'), 'on'),

            'creator_id' => $request->user()->id

        ];

        $answers = $request->get('answer', []);

        foreach ($answers as $index => $answer) {

            if (empty($answer)) {

                unset($answers[$index]);

            }

        }

        return [$data, $answers];

    }
    // =============== END OF CREATE POLL =======================

    // ================ EDIT POLL ============

    /**
     *
     * Method that handles GET request and shows edit page for single poll
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function editPoll(Poll $poll)
    {

        return view('admin.poll', ['poll' => $poll, 'edit_answers' => $poll->usersAnswers->isEmpty()]);

    }

    /**
     *
     * Method that handles POST request and its used to update poll title, question and answers if possible
     *
     * @param Request $request
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function updatePoll(Request $request, Poll $poll)
    {

        $this->validatePollInput($request);

        if ($poll->usersAnswers->isEmpty()) {

            $poll->updateAnswers($request);

        }

        $poll->title = $request->get('title', $poll->title);

        $poll->question = $request->get('question', $poll->question);

        $poll->is_public = 0 === strcasecmp($request->get('is_public', 'off'), 'on');

        $poll->is_active = 0 === strcasecmp($request->get('is_active', 'off'), 'on');

        $responseData = [

            'message' => 'Poll failed to save.',

            'edit_answers' => $poll->usersAnswers->isEmpty()

        ];

        if ($poll->save()) {

            $responseData['message'] = 'Poll successfully saved';

        }

        $responseData['poll'] = $poll->fresh();

        return view('admin.poll', $responseData);

    }

    public function delete(Poll $poll)
    {

        /** @var UserPollAnswer $userAnswer */
        foreach ($poll->usersAnswers as $userAnswer) {

            $userAnswer->delete();

        }

        /** @var PollAnswer $answer */
        foreach ($poll->answers as $answer) {

            $answer->delete();

        }

        $poll->delete();

        return redirect()->route('admin.poll.list');

    }
    // ================ END OF EDIT POLL ================



    // =============== SHARED BETWEEN METHODS ===========
    private function validatePollInput(Request $request)
    {

        $this->validate($request, [

            'title' => 'required|max:256',

            'question' => 'required'

        ]);

    }

}