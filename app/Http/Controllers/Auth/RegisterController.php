<?php

namespace App\Http\Controllers\Auth;

use App\Models\UserRole;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        $userRole = new UserRole([

            'role_type' => (string) UserRole::ROLE_USER,

            'user_id' => $user->id

        ]);

        if ($user && $user->exists && $userRole->save()) {

            Mail::send('email.activation', ['user' => $user], function ($m) use ($user) {

                $m->from('poll@example.com', 'Poll');

                $m->to($user->email, 'User')->subject('Activation Code');

            });

            return view('auth.login', ['message' => 'Activation link sent to your email address.']);

        }

        return view('auth.login', ['errorMessage' => 'Failed to register user, please try again later.']);
    }

    public function activate(Request $request, $activationCode)
    {

        $user = User::where('code', $activationCode)->first();

        if (null === $user || false === $user->exitst) {

            return view('auth.login', ['errorMessage' => 'Invalid activation code.']);

        }

        $user->status = User::STATUS_ACTIVE;

        $user->code = '';

        if ($user->save()) {

            return view('auth.login', ['message' => 'User activated, you can now login.']);

        }

        return view('auth.login', ['errorMessage' => 'Error activating user, please try again later.']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => User::STATUS_INACTIVE,
            'code' => hash('sha256', uniqid('', true))
        ]);
    }
}
