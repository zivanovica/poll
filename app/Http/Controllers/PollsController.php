<?php
/**
 * Created by PhpStorm.
 * User: coaps
 * Date: 9.4.2017.
 * Time: 17.28
 */

namespace App\Http\Controllers;

use App\Models\Poll;
use App\Models\PollAnswer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PollsController extends Controller
{

    /**
     *
     * Show user all public poll responses
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userResults(Request $request)
    {

        $polls = Poll::getUserPollResults($request->user());

        foreach ($polls as $poll) {

            $this->calculatePollPercentage($poll);

        }

        return view('polls.results', ['polls' => $polls]);

    }

    /**
     *
     * Show user single public poll results
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pollResults(Poll $poll)
    {

        $this->calculatePollPercentage($poll);

        return view('polls.result', ['poll' => $poll]);

    }


    /**
     *
     * Attach "percentage" value to each answer in poll
     *
     * @param Poll $poll
     * @return Poll
     */
    private function calculatePollPercentage(Poll $poll)
    {

        $userAnswers = $poll->usersAnswers;

        $total = $userAnswers->count();

        /** @var PollAnswer $answer */
        foreach ($poll->answers as $index => $answer) {

            $poll->answers[$index]->totalVotes = $userAnswers->where('poll_answer_id', $answer->id)->count();

            $poll->answers[$index]->percentage = 0 === $total ? 0 : $poll->answers[$index]->totalVotes / $total * 100;

        }

        return $poll;

    }

    /**
     *
     * List all available polls to user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function polls(Request $request)
    {

        $polls = Poll::getUserVotablePolls($request->user());

        return view('polls.list', ['polls' => $polls]);

    }

    /**
     *
     * Show single poll
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function poll(Poll $poll)
    {

        return view('polls.vote', ['poll' => $poll]);

    }

    /**
     *
     * Vote for poll answer
     *
     * @param Request $request
     * @param Poll $poll
     * @return \Illuminate\Http\RedirectResponse
     */
    public function vote(Request $request, Poll $poll)
    {

        $answer = $poll->answers->where('id', $request->get('answer', 0))->first();

        if (null === $answer || false === $answer->exists()) {

            return $this->redirectToPoll($request->user(), $poll, ['message' => 'Invalid answer.']);

        }

        $userAnswer = $poll->createUserVote($request->user(), $answer);

        if ($userAnswer->save()) {

            return $this->redirectToPoll($request->user(), $poll, ['message' => 'Vote success']);

        }

        return $this->redirectToPoll($request->user(), $poll, ['message' => 'Vote failed']);

    }

    /**
     *
     * Redirect user to poll result if possible, otherwise user gets redirected to polls list
     *
     * @param Poll $poll
     * @param array $parameters
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectToPoll(User $user, Poll $poll, array $parameters = [])
    {

        if ($poll->is_public || $user->r) {

            return redirect()->route('poll.results', array_merge($parameters, ['poll' => $poll]));

        }

        return redirect()->route('poll.list', $parameters);

    }

}