<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ValidateUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        if (null === $user || false === $user->exists) {

            return $next($request);

        }

        if ($user->status !== User::STATUS_ACTIVE) {

            Auth::logout();

            return response(view('auth.login', ['errorMessage' => "Your account is {$user->status}"])->render(), 403);

        }

        return $next($request);

    }
}
