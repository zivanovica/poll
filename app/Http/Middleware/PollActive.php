<?php

namespace App\Http\Middleware;

use App\Models\UserRole;
use Closure;
use Illuminate\Http\Request;

class PollActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->user()->isAdmin()) {

            return $next($request);

        }

        $poll = $request->route('poll');

        if (null === $poll || false === (bool) $poll->is_active) {

            return redirect()->route('poll.list');

        }

        return $next($request);
    }
}
