<?php

namespace App\Http\Middleware;

use App\Models\Poll;
use App\Models\UserRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PollVoteAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->user()->isAdmin()) {

            return redirect()->route('poll.list');

        }

        /** @var Poll $poll */
        $poll = $request->route('poll');

        if (Gate::denies('poll-can-vote', $poll)) {

            return redirect()->route('poll.list');

        }

        return $next($request);

    }
}
