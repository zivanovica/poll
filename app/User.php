<?php

namespace App;

use App\Models\UserPollAnswer;
use App\Models\UserRole;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;

    const STATUS_INACTIVE = 'inactive';

    const STATUS_ACTIVE = 'active';

    const STATUS_DISABLED = 'disabled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'code', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function createAdministrator($email, $password)
    {
        $user = User::create([
            'email' => $email,
            'password' => bcrypt($password ),
            'status' => User::STATUS_ACTIVE,
            'code' => ''
        ]);

        $role = UserRole::create([

            'user_id' => $user->id,

            'role_type' => UserRole::ROLE_ADMIN

        ]);

        return $user->exists && $role->exists;

    }

    public static function getStatuses()
    {

        return [

            User::STATUS_INACTIVE,

            User::STATUS_ACTIVE,

            User::STATUS_DISABLED,

        ];

    }

    public function role()
    {

        return $this->hasOne(UserRole::class, 'user_id', 'id');

    }

    public function answers()
    {

        return $this->hasMany(UserPollAnswer::class, 'user_id', 'id');

    }

    public function isAdmin()
    {

        return $this->role->role_type === UserRole::ROLE_ADMIN;

    }
}
