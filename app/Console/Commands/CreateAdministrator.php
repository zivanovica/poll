<?php

namespace App\Console\Commands;

use App\Http\Controllers\Auth\RegisterController;
use App\Models\UserRole;
use App\User;
use Illuminate\Console\Command;

class CreateAdministrator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new administrator user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userRole = UserRole::where('role_type', UserRole::ROLE_ADMIN)->first();

        if ($userRole instanceof UserRole && $userRole->exists) {

            throw new \InvalidArgumentException('Administrator user already exists.');

        }

        if (User::createAdministrator('polladmin@cloudhorizon.com', 'polladmin')) {

            echo "\nAdministrator successfully created.\n";

            return 0;

        }

        echo "\nFailed to create administrator.";

        return 1;
    }
}
